;;; Org-Functions --- Functions to be used by org configurations

;;; Commentary:

;; Constants and variables

(setq-default *org-directory* (file-truename "~/org"))

(setq-default org-agenda-autoupdate-timer nil)
(setq-default org-agenda-autoupdate-toggle nil)
(setq-default org-agenda-autoupdate-seconds 60)

(defvar org-element-hooked-buffers nil)

;;; Macros

(defmacro set-named-timer (symbol fn repeat)
  "Create timer routine that run FN every REPEAT seconds and save it in SYMBOL.

If called more than once this will cancel the timer using `cancel-timer' before
creating a new one."
  (let* ((current-second (car (decode-time)))
         (start-after (- 60 current-second)))
    `(progn
       (and (boundp ',symbol) ,symbol (cancel-timer ,symbol))

       (setq ,symbol (run-with-timer ,start-after ,repeat #',fn)))))

;;; Code:

;; Extends org-agenda functions

(defun org-agenda-autoupdate (enable)
  "Toggle autoupdate for Org Agenda.

If ENABLE is non-nil then start updating the buffer every
`org-agenda-autoupdate-seconds'."
  (interactive
   (list
    (let ((prompt (if org-agenda-autoupdate-toggle
                      "Stop Org Agenda autoupdate? "
                    "Start Org Agenda autoupdate? ")))
      (if (y-or-n-p prompt)
          (not org-agenda-autoupdate-toggle)
        (user-error "Abort!")))))

  (setq org-agenda-autoupdate-toggle (not (eq enable nil)))

  (message (format "Agenda autoupdate %s!" (if org-agenda-autoupdate-toggle "started" "stopped")))

  (if org-agenda-autoupdate-toggle
      (set-named-timer org-agenda-autoupdate-timer org-fn-update-agenda org-agenda-autoupdate-seconds)
    (and org-agenda-autoupdate-timer (cancel-timer org-agenda-autoupdate-timer))))

;; org helper functions

(defun org-fn-update-agenda-from-file ()
  "Check if any org file changed in disk and force an update on *Org Agenda*.

Do nothing if no Org Agenda is active.

If updated returns the *Org Agenda* buffer."
  (interactive)

  (require 'org-agenda)

  (when-let (agenda-buffer (get-buffer "*Org Agenda*"))
    (and
     (buffer-live-p agenda-buffer)

     (seq-filter
      #'(lambda (orig-buffer)
          (when (member (buffer-file-name orig-buffer) org-agenda-files)
            (when (save-restriction
                    (widen)

                    (with-temp-buffer
                      (insert-file-contents (buffer-file-name orig-buffer))

                      (not (zerop (compare-buffer-substrings orig-buffer nil nil (current-buffer) nil nil)))))

              (with-current-buffer orig-buffer
                (revert-buffer t t))
              t)))

      (buffer-list))

     (progn
       (with-selected-window (get-buffer-window agenda-buffer t)
         (let ((inhibit-message t))
           (org-agenda-redo t)))

       agenda-buffer))))

(defun org-fn-update-agenda ()
  (interactive)

  (require 'org-agenda)

  (when-let* ((agenda-buffer (get-buffer "*Org Agenda*"))
              (window (get-buffer-window agenda-buffer t)))
    (with-selected-window window
      (let ((inhibit-message t))
        (org-agenda-redo t)))))

(defun org-fn-list-agenda-files ()
  "List org files that keyword `#+AGENDA' is t.

Note: This function requires `org-element' to work."
  (interactive)

  (require 'org-element)

  (seq-filter
   (lambda (filename)
     (with-temp-buffer
       (insert-file-contents filename nil 0)

       (car (org-element-map (org-element-parse-buffer) 'keyword
              (lambda (el)
                (and (string-match "AGENDA" (org-element-property :key el)) (member (org-element-property :value el) '("t" "yes")) t))))))

   (directory-files-recursively *org-directory* "\.org$")))

(defun org-element--print-debug ()
  "Open a new window and print org-element structure in it."
  (when (cl-member (buffer-name) org-element-hooked-buffers :test #'string=)
    (message "Debugging: %s" (current-buffer))

    (let ((bufname (buffer-name))
          (parsed-structure (org-element-parse-buffer))
          (debug-buffer (get-buffer-create "*Org Element Debug*")))
      (with-help-window debug-buffer
        (emacs-lisp-mode)
        (erase-buffer)
        (goto-char (point-min))
        (insert (format ";; Dump from file \"%s\"\n\n" bufname))
        (insert (format "%s" parsed-structure))
        (visual-line-mode 1)))))

(defun org-element-toogle-debug-buffer (buffer-or-name)
  "Toogle debug buffer for BUFFER-OR-NAME.

This function add calls `org-element--print-debug' on
`after-save-hook' of specified file."
  (interactive "bSelect a buffer to dump: ")

  (with-current-buffer buffer-or-name
    (let ((bufname (if (stringp buffer-or-name) buffer-or-name (buffer-name buffer-or-name))))
      (if (cl-member bufname org-element-hooked-buffers :test #'string=)
          (setq org-element-hooked-buffers (remove bufname org-element-hooked-buffers))
        (push bufname org-element-hooked-buffers))

      (if org-element-hooked-buffers
          (unless (member #'org-element--print-debug after-save-hook)
            (add-hook 'after-save-hook #'org-element--print-debug :LOCAL))
        (remove-hook 'after-save-hook #'org-element--print-debug :LOCAL)))))

(provide 'org-functions)

;;; org-functions.el ends here
