;;; Init --- Emacs' init script file

;;; Commentary:

;;; Code:

;;; Config files

;; Add local package folder to load path
(add-to-list 'load-path (file-truename "~/.emacs.d/libs/"))

;; Set path for `custom-set-faces' and `custom-set-variables'.
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(load custom-file t)

;; Set org default folder path as `$HOME/org'.
(defvar *org-directory* (file-truename "~/org")
  "Default org folder path.
It will be used to open or search org related files.")

;; Set org-roam default folder path.
(defvar org-roam-directory (file-truename (concat *org-directory* "/roam-files"))) ;; org-roam-directory path as `$HOME/org/roam-files'.

;;; Warnings

;; Disable warnings during compile.
(setq byte-compile-warnings '(cl-functions))

;;; Backup

;; Set default backup folder to store unsaved emacs's buffers.
(setq backup-directory-alist '(("." . "~/.emacs-backup")))

;;; Package

;; Load package manager
(require 'package)

;; Add melpa to package archives.
;; This databases will be used to download and maintain the installed packages.
;; Is needed to add to the variable `package-archive' associated list being the
;; first value the internal name and the second the archive URL.
(add-to-list 'package-archives
             '("stable" . "http://stable.melpa.org/packages/"))
(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/"))

;; Define which order the packages are selected to (re)install a package.
;; The current order is `stage' -> `gnu' -> `melpa' -> `nongnu'.
(setq package-archive-priorities
      '(("stable" . 10)
        ("gnu" . 9)
        ("melpa" . 8)
        ("nongnu" . 7)))

(package-initialize)

(unless package-archive-contents
  (package-refresh-contents))

;; Install the `use-package' if not exists
(unless (package-installed-p 'use-package)
  (package-install 'use-package))

;;; Config files

;; Init config-files variable
(defvar config-files '())

;; Add custom configuration files.
;; The value added to this variable should be an associated list which the head
;; is the folder path and the rest the filenames without the extension '.el'
(add-to-list 'config-files
             '("~/.emacs.d/config" . ("common" "windows-setup" "python" "ssh"
				      "minimap" "sql" "undo-tree" "csv" "css"
				      "elisp" "markdown" "org" "treemacs" "js")))

;;; Custom auto mode

(add-to-list 'auto-mode-alist '("\\.py$" . python-mode))
(add-to-list 'auto-mode-alist '("\\.cpp$" . c++-mode))
(add-to-list 'auto-mode-alist '("\\.c++$" . c++-mode))
(add-to-list 'auto-mode-alist '("\\.h$" . c++-mode))
(add-to-list 'auto-mode-alist '("\\.thtml$" . html-mode))
(add-to-list 'auto-mode-alist '("\\.thtm$" . html-mode))
(add-to-list 'auto-mode-alist '("\\.wiki$" . html-mode))
(add-to-list 'auto-mode-alist `("\\.ts" . typescript-mode))
(add-to-list 'auto-mode-alist `("\\.tsx" . typescript-mode))

;;; Agenda

;; (setq-default appt-display-format 'window)
;; (setq-default appt-disp-window-function (function appt-trigger-alert))

;; (defun appt-trigger-alert (min-to-app new-time msg)
;;   "Beep.

;; Arg `MIN-TO-APP', `NEW-TIME' and `MSG' are ignored."
;;   (beep-alert))

;; Appt windows notifier

;; (defun custom-org-agenda-to-appt ()
;;   "Rebuild all appt reminders."
;;   (interactive)
;;   (setq-default appt-time-msg-list nil)
;;   (run-at-time "24:01" nil 'my-org-agenda-to-appt)
;;   (let ((org-deadline-warning-days 0))
;;     (org-agenda-to-appt)))

;; ;; Disabled so that I can open multiple emacs without org loading agenda files
;; (appt-activate t)

;; ;; 5 minutes warning
;; (setq-default appt-message-warning-time '60)
;; ;; (setq appt-display-duration '15)
;; (setq-default appt-display-interval '15)

;; ;; Update appt each time agenda is opened
;; (add-hook 'org-finalize-agenda-hook 'custom-org-agenda-to-appt)

;;; Gherkin

(defvar feature-default-language "pt")

;;; Load config files

;; Reads `config-files' variable and load the listed files.
(let ((load-start-time (float-time)))
  (dolist (config config-files)
    (let ((folder (file-name-as-directory (car config))))
      (dolist (file (cdr config))
	(let ((filename (concat folder (format "%s.el" file)))
              (start-time (float-time)))
          (load-file filename)
          (message "Loaded in %f seconds" (- (float-time) start-time))))))
  (message "All files loaded in %f seconds" (- (float-time) load-start-time)))

;;; init.el ends here
