;;; SQL --- Config script file

;;; Commentary:

;;; Code:

;;; Variables definition

(defconst sql--prompt-prefix-buffer "Parsing SQL buffer."
  "Prefix for prompt in buffer SQL parsing.")

(defconst sql--prompt-prefix-region "Parsing SQL region."
  "Prefix for prompt in region SQL parsing.")

;;; SQL handling functions

(defun sql--sqlparse-command-region (begin end wrap-after keyword-parse)
  "Call python script for formatting SQL queries.

The parameters BEGIN and END are the region points to apply
the string formatting.

The parameter WRAP-AFTER will limit the column wrapping size
for each line and defaults for the value 80.

The value for KEYWORD-PARSE will apply a modifier to each
keyword in the SQL text."
  (let* ((script-file (file-truename (concat user-emacs-directory "scripts/sql_format.py")))
	 (params (format "--wrap-after %d" wrap-after))
	 (params (if keyword-parse (format "%s --keyword-case %s" params keyword-parse) params))
	 (cmd (format "python3 \"%s\" %s" script-file params)))

    (shell-command-on-region begin end cmd nil t)))

(defun sql--ask-wrap-line (prompt-prefix)
  "Ask in minibuffer for wrap line size.

Accepts PROMPT-PREFIX to concatenate to prompt message as prefix.

This function returns the value typed in the minibuffer."
  (let ((prompt (format "%s\nWrap line after: " prompt-prefix)))
    (read-number prompt 80)))

(defun sql--ask-keyword-parse (prompt-prefix)
  "Ask in minibuffer for keyword parse style.

Accepts PROMPT-PREFIX to concatenate to prompt message as prefix.

This function returns the options `upper', `lower', `capitalize' or nil."
  (let* ((prompt (propertize (format "%s\nParse keywords to: " prompt-prefix) 'face 'minibuffer-prompt))
	 (choice (read-multiple-choice prompt
				       '((?u "UPPER" "Parse all keywords to uppercase.")
					 (?l "lower" "Parse all keywords to lowercase.")
					 (?c "Capitalize" "Capitalize all keywords.")
					 (?n "none" "Just do nothing.")))))
    (pcase (car choice)
      (?u "upper")
      (?l "lower")
      (?c "capitalize"))))

(defun sqlparse-region (wrap-after keyword-parse)
  "Formats a SQL text in the marked area.

The parameter WRAP-AFTER will limit the column wrapping size
for each line, defaults to `80'.

The value for KEYWORD-PARSE will apply a modifier to each
keyword in the SQL text."
  (interactive
   (list (sql--ask-wrap-line sql--prompt-prefix-region)
	 (sql--ask-keyword-parse sql--prompt-prefix-region)))

  (sql--sqlparse-command-region (region-beginning) (region-end) wrap-after keyword-parse))


(defun sqlparse-buffer (wrap-after keyword-parse)
  "Formats an entire buffer as SQL text.

The parameter WRAP-AFTER will limit the column wrapping size
for each line, defaults to `80'.

The value for KEYWORD-PARSE will apply a modifier to each
keyword in the SQL text."
  (interactive
   (list (sql--ask-wrap-line sql--prompt-prefix-buffer)
	 (sql--ask-keyword-parse sql--prompt-prefix-buffer)))

  (sql--sqlparse-command-region (point-min) (point-max) wrap-after keyword-parse))

;;; sql-mode Config

(use-package sql
  :config
  (define-key sql-mode-map (kbd "C-c i") 'sqlparse-region)
  (define-key sql-mode-map (kbd "C-c M-i") 'sqlparse-buffer))

;;; sql.el ends here
