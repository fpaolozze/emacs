;;; CSV --- Config script file

;;; Commentary:

;;; Code:

(use-package csv-mode
  :ensure t
  :init
  (defun csv-styling ()
    "CSV readable style."
    (csv-align-fields nil (point-min) (point-max))
    (csv-header-line t))
  :hook (csv-mode-hook . csv-styling)
  :custom
  (csv-separators '("|" ";")))

;;; csv.el ends here
