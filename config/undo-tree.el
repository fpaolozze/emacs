;;; Undo-Tree --- Config file script

;;; Commentary:

;;; Code:

(use-package undo-tree
  :ensure t
  ;; Enable undo-tree for specific modes
  :hook ((python-mode emacs-lisp-mode typescript-mode web-mode) . undo-tree-mode)
  :custom
  ;; Set custom undo-tree folder path
  (undo-tree-history-directory-alist '(("" . "~/.emacs-backup"))))

;;; undo-tree.el ends here
