;;; Minimap --- Config script file

;;; Commentary:

;;; Code:

(use-package minimap
  :ensure t
  :custom
  (minimap-window-location 'right))

;;; minimap.el ends here
