;;; CSS --- Config file script

;;; Commentary:

;;; Code:

;;; rainbow-mode config hook

(use-package rainbow-mode
  :ensure t
  :hook (css-mode . rainbow-mode))

;;; Review this

;; (defvar cssm-indent-level 4)
;; (defvar cssm-newline-before-closing-bracket t)
;; (defvar cssm-mirror-mode nil)

;;; css.el ends here
