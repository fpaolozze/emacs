;;; Elisp --- Config file script

;;; Commentary:

;;; Code:

(use-package emacs-lisp
  :hook ((emacs-lisp-mode . display-line-numbers-mode)
	 (emacs-lisp-mode . (lambda () (setq-local indent-tabs-mode nil)))))

(use-package company
  :ensure t
  :pin melpa
  :hook (emacs-lisp-mode . company-mode)
  :config
  (define-key company-mode-map (kbd "C-M-i") 'company-capf))

(use-package flycheck
  :ensure t
  :hook (emacs-lisp-mode . flycheck-mode))

;;; Config related

;; Open the config file itself
(defun customize-emacs ()
  "Customize .emacs file."
  (interactive)
  (find-file user-init-file))

;;; elisp.el ends here
