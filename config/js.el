;;; JS --- Config script file

;;; Commentary:

;;; Code:

;;; Typescript config
(use-package typescript
  :hook (typescript-mode . display-line-numbers-mode)
  :custom
  (typescript-indent-level 2))

;;; Typescript .tsx config
(use-package web-mode
  :hook (web-mode . display-line-numbers-mode)
  :custom
  (web-mode-markup-indent-offset 2)
  (web-mode-css-indent-offset 2)
  (web-mode-code-indent-offset 2)
  (web-mode-block-padding 2)
  (web-mode-comment-style 2)

  (indent-tabs-mode nil)

  (web-mode-enable-css-colorization t)
  (web-mode-enable-auto-pairing t)
  (web-mode-enable-comment-keywords t)
  (web-mode-enable-current-element-highlight t))

;;; js.el ends here
