;;; Markdown --- Config script file

;;; Commentary:

;;; Code:

;; Markdown config
(use-package markdown-mode
  :ensure t
  :init
  ;; Add `README' filename to makedown-mode hook
  (add-to-list 'auto-mode-alist '("\\README$" . markdown-mode)))

;; Checks if markdownlint executable exists and configure as lint checker
(use-package flycheck
  :requires markdown-mode
  :if (executable-find "markdownlint")
  :hook (markdown-mode . flycheck-mode)
  :config
  (flycheck-set-checker-executable 'markdown-markdownlint-cli "markdownlint"))

;;; markdown.el ends here
