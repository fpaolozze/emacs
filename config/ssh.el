;;; SSH --- Config script file

;;; Commentary:

;;; Code:

;; Set tramp method to plink on Windows

(if (eq system-type 'windows-nt)
    (defvar tramp-default-method "plink"))

;; Disable ssh validation before save
(defvar tramp-use-ssh-controlmaster-options nil)

;; Handle remote directories as local
(setq enable-remote-dir-locals t)

;;; ssh.el ends here
