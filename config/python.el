;;; Python --- Config script file

;;; Commentary:

;;; Code:

;;; Setup packages

;; Try to disable default `python-mode'
(use-package python
  :disabled)

;; Setup `python-mode' package
(use-package python-mode
  :hook
  ((python-mode . flycheck-mode)
   (python-mode . display-line-numbers-mode))
  :custom
  (flycheck-python-flake8-executable "flake8"))

;; Setup `blacken' package
(use-package blacken
  :ensure t
  :custom
  (blacken-line-length 80)
  (blacken-skip-string-normalization t)) ; Skip string normalization by default

;;; python.el ends here
