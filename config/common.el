;;; Common --- Config file script

;;; Commentary:

;;; Code:

;;; Coding helpers

;; Call to hook on `before-save' only when `emacs-lisp-mode' or `python-mode' is active in current buffer
(defun delete-trailing-whitespace-on-save-hook ()
  "Delete whitespace only if in `emacs-list-mode' or `python-mode'."
  (when (member major-mode '(emacs-lisp-mode python-mode))
    (delete-trailing-whitespace)))

;; Call for function `delete-trailing-whitespace-on-save-hook' when save hook is called
(add-hook 'before-save-hook 'delete-trailing-whitespace-on-save-hook)

;; Search in Google for a specific term
(defconst search-modes '((emacs-lisp-mode . "emacs")
			 (lisp-interaction-mode . "emacs")
			 (python-mode . "python")))
(defvar search-url "https://google.com/search?q=%s \"%s\"")

(defun search-symbol (type term)
  "Open the browser and search in Google for some `term'.

The argument TYPE receive an option to apply a filter to `term',
default to the current mode.

The TERM is the string to be searched, defaults to the symbol at point."
  (interactive
   (let* ((symbol (if-let (sym (symbol-at-point)) (symbol-name sym)))
	  (term (read-from-minibuffer "Search in google: " symbol))
	  (type (completing-read "Language: " (mapcar 'cdr search-modes) nil t (alist-get major-mode search-modes))))
     (list type term)))
  (browse-url (format search-url type term)))

(define-key help-map "g" 'search-symbol) ; Set keybinding to C-h g

;;; Buffer handling

;; Set fill-mode column count
(setq fill-column 80)

;; Disable auto fill
(auto-fill-mode -1)

;; Prompt and revert buffer from file
(defun revert-buffer-no-confirm ()
  "Revert current buffer (reload from file)."
  (interactive)
  (when (y-or-n-p "Revert buffer? ")
    (revert-buffer :ignore-auto :noconfirm)))

;; Auto indent current buffer and save the file
(defun indent-buffer ()
  "Auto indent the buffer."
  (interactive)
  (save-excursion (indent-region (point-min) (point-max) nil)))

;; Activate ido-mode globally
;; This mode helps to search buffers and files in an interactive way
(use-package ido
  :ensure t
  :init
  (ido-mode 'both)
  ;; Ignoring Org Archive file on call `find-file'
  (add-to-list 'ido-ignore-files ".*\.org_archive"))

;; Disable indent with tabs, only spaces
(setq indent-tabs-mode nil)

;;; Encoding config

;; Set clipboard paste encoding type.
;; If the OS Windows uses UTF-16 otherwise uses UTF-8.
;; This is because Windows does not understand UTF-8 correctly.
(let ((name (if (eq system-type 'windows-nt) 'utf-16-le 'utf-8-unix)))
  (set-selection-coding-system name))

(setq coding-system-for-write 'utf-8-unix)

;;; Global keybindings

;; Revert buffer and reload contents from file
(global-set-key [f5] 'revert-buffer-no-confirm)

;; Toggle a minimap for the current buffer
(global-set-key [f6] 'minimap-mode)

;; Open or focus to treemacs lateral bar
(global-set-key [f7] 'treemacs-select-window)

;; Don't know what this does, research about later
(global-set-key "\177" 'backward-delete-char-untabify)

;;; User interaction

;; Disable top menu
(menu-bar-mode -1)

;; Disable splash
(setq inhibit-startup-screen t)

;; Replace audible alert by visible alert (disabled because is annoying)
;; (setq visible-bell t)

;; Disable audible alert
(setq ring-bell-function 'ignore)

;; Mouse handling
(mouse-wheel-mode)
(mouse-avoidance-mode 'jump)

;;; Enable mouse when X-Term
(unless (display-graphic-p)
  (global-set-key [f8] 'xterm-mouse-mode))

;; Enable column-number-mode
(column-number-mode t)

;; alert-toast config
(use-package alert
  :commands alert
  :custom
  (alert-default-style 'toast))

(use-package alert-toast
  :if (eq system-type 'windows-nt)
  :after alert)

;; Set time display on emacs as 24h format
(defvar display-time-24hr-format t)

;; Hide toolbar (top menu) from emacs
(tool-bar-mode -1)

;;; Registers

;; Zenvia book registers
(set-register ?s (cons 'file "/plink:dev-sense:"))

;;; Snippets

(use-package yasnippet
  :ensure t
  :defer t
  :custom
  ;; Define the default snippets folder
  (yas-snippet-dirs '("~/.emacs.d/snippets"))
  :config
  ;; Create snippets directories
  (dolist (dir-name yas-snippet-dirs)
    (make-directory dir-name t)))

;;; Magit

(use-package magit
  :ensure t
  :pin melpa
  :init
  (defun magit-org-stage-and-push ()
    (interactive)

    (require 'magit-process)
    (require 'magit-git)
    (when (file-directory-p *org-directory*)
      (message "Pushing org files to remote...")

      (let ((default-directory *org-directory*))
        (when (magit-anything-modified-p)
          (magit-status default-directory)
          (magit-stage-modified nil)
          (magit-commit-create `("-m" ,(concat "Files Snap - " (format-time-string "%Y-%m-%dT%T")))))

        (magit-push-current-to-pushremote nil)
        (magit-mode-bury-buffer nil))

      (message "Pushing org files to remote... Done")))

  (defun magit-org-stage-and-pull ()
    (interactive)

    (require 'magit-process)
    (require 'magit-git)
    (when (file-directory-p *org-directory*)
      (message "Pulling org files from remote...")

      (let ((default-directory *org-directory*))
        (if (magit-anything-modified-p)
            (user-error "Cannot pull from remote server, one or more files are modified locally"))

        (magit-status default-directory)
        (magit-pull-from-upstream nil)
        (magit-mode-bury-buffer nil))

      (message "Pulling org files from remote... Done")))
  :bind
  ("C-c o P" . magit-org-stage-and-push)
  ("C-c o F" . magit-org-stage-and-pull))


;;; Helper functions

;; UUID generator
(use-package uuidgen
  :ensure t)

;;; Lua

(use-package lua-mode
  :ensure t
  :defer t
  :pin melpa)

;;; Flycheck

(use-package flycheck-popup-tip
  :ensure t
  :requires flycheck
  :hook (flycheck-mode . flycheck-popup-tip-mode))

;; Custom beep-alert
(defun beep-alert ()
  "Just emits an alert sound on Windows."
  (interactive)

  (not (zerop (cond
               ((eq system-type 'windows-nt) (call-process "powershell.exe" nil nil nil "-Command" "[system.media.systemsounds]::Hand.play()"))
               ((eq system-type 'gnu/linux) (call-process "echo" nil nil nil "-ne" "'\007'"))))))

;;; Files session store

(use-package easysession
  :ensure t
  :hook (kill-emacs . easysession-save)
  :bind ("C-c s l" . easysession-load)
  :custom
  (easysession-save-interval (* 2 60))
  :init
  (add-hook 'emacs-startup-hook #'easysession-save-mode 99))

(use-package savehist
  :hook
  (after-init . savehist-mode)
  :config
  (add-to-list 'savehist-additional-variables 'kill-ring))

;;; common.el ends here
