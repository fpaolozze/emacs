;;; Windows-setup --- Windows setup script file

;;; Commentary:

;;; Code:

(when (eq system-type 'windows-nt)
  ;; Using cywin64's find command
  (setq find-program "c:/cygwin64/bin/find.exe")

  ;; Temp fix WSL
  (defun fp/ignore-wsl-acls (orig-fun &rest args)
    "Ignore ACLs on WSL.

WSL does not provide an ACL, but emacs
expects there to be one before saving any file. Without this
advice, files on WSL can not be saved."
    (if (string-match-p "^//wsl\$/" (car args))
        (progn (message "ignoring wsl acls") "")
      (apply orig-fun args)))

  (advice-add 'file-acl :around 'fp/ignore-wsl-acls))

;;; windows-setup.el ends here
