;;; Treemacs --- Config file script

;;; Commentary:

;;; Code:

;;; Base treemacs config

(use-package treemacs
  :ensure t
  :hook (treemacs-mode . treemacs-follow-mode)
  :custom
  (treemacs-indentation 2) ;; Make folder indentation 2
  (treemacs-width 30)
  :config
  (treemacs-filewatch-mode t) ;; Activate filewatch
  (treemacs-hide-gitignored-files-mode t) ;; Hide files based by .gitignore in project

  ;; Define a predicate function to ignore common python files
  (defun treemacs-hide-python-files-p (filename abs-path)
    (member filename '(".pytest_cache" "venv" ".coverage" "__pycache__")))
  (add-to-list 'treemacs-ignored-file-predicates 'treemacs-hide-python-files-p))

;;; Treemacs magit support
(use-package treemacs-magit
  :after (treemacs magit)
  :ensure t)

;;; treemacs.el ends here
