;;; Org --- Config file script

;;; Commentary:

;;; Requirements:

(require 'org-functions)

;;; Code:

;;; Org config
(use-package org
  :ensure t
  :bind (("C-c l" . org-store-link)
         ("C-c c" . org-capture))
  :hook (org-mode . display-line-numbers-mode)
  :init
  ;; Set registers
  (set-register ?p (cons 'file (concat *org-directory* "/Pessoal.org")))
  :custom
  (org-journal-dir (concat *org-directory* "/journal"))
  (org-default-notes-file (concat *org-directory* "/notes.org"))
  (org-log-done t)
  (org-display-custom-times t)
  (org-time-stamp-custom-formats '("<%Y-%m-%d>" . "<%Y-%m-%d %H:%M>"))

  ;; Set custom colors for TODO flags
  (org-todo-keyword-faces '(("RFC" . "yellow")
			    ("HOLD" . "orange")
			    ("WIP" . "yellow")
			    ("CANCELLED" . "gray"))))

(use-package org-bullets
  :ensure t
  :after org
  :hook (org-mode . org-bullets-mode))

;;; Org+verb config

(use-package verb
  :ensure t
  :config
  (define-key org-mode-map (kbd "C-c C-r") verb-command-map)

  (org-babel-do-load-languages
   'org-babel-load-languages
   '((emacs-lisp . t)
     (python . t)
     (verb . t)
     (sql . t))))

;;; Org+company config

(use-package org
  :hook (org-mode . company-mode)
  :config
  (push 'md org-export-backends))

;;; Org Agenda

;; org-agenda config
(use-package org
  :bind (("C-c a" . org-agenda))
  :hook (after-save . (lambda ()
                        (let ((buf-name (buffer-file-name (current-buffer))))
                          (when (and (eq major-mode 'org-mode) (member buf-name org-agenda-files))
                            (org-fn-update-agenda)))))
  :config
  ;; Set org-agenda files
  (setq org-agenda-files (org-fn-list-agenda-files))

  (let ((counter 1))
    (dolist (buffer (seq-filter (lambda (buffer) (member (buffer-file-name buffer) org-agenda-files)) (buffer-list)))
      (with-current-buffer buffer
        (bookmark-set (format "Agenda files %d" counter)))

      (setq counter (+ counter 1))))

  ;; PT-BR calendar
  (defvar calendar-week-start-day 0)
  (defvar calendar-day-name-array ["Domingo" "Segunda" "Terca" "Quarta"
                                   "Quinta" "Sexta" "Sabado"])
  (defvar calendar-month-name-array ["Janeiro" "Fevereiro" "Março" "Abril"
                                     "Maio" "Junho" "Julho" "Agosto"
                                     "Setembro" "Outubro" "Novembro" "Dezembro"])

  ;; Auto update agenda
  ;; (set-named-timer agenda-update-timer org-fn-update-agenda-buffer 300)
  )

;; org-super-agenda config
;; (use-package org-super-agenda
;;   :ensure t
;;   :hook (org-agenda-mode . org-super-agenda-mode)
;;   :custom
  ;; (org-super-agenda-groups
  ;;  ;; List all entries by the tag TRACK or todo as HOLD
  ;;  '((:name "Acompanhar"
  ;;           :tag "TRACK"
  ;;           :todo "HOLD")
  ;;    ;; List all entries by A priority
  ;;    (:name "Prioritário"
  ;;           :priority>= "B")

  ;;    ;; List all entries by the tag RELEASE
  ;;    (:name "Marcados p/ release"
  ;;           :tag "RELEASE")

  ;;    (:auto-group t)
  ;;    (:auto-category t))))

;; brazilian-holidays config
(use-package brazilian-holidays
  :ensure t
  :custom
  (brazilian-holidays-sp-holidays t)
  :config
  ;; Add the date XXXX-01-25 as Sao Paulo anniversary
  (add-to-list 'brazilian-holidays--local-holidays
	       '(holiday-fixed 1 25 "Aniversário de São Paulo"))

  ;; This list of days generate all 5 days of Carnaval, which the
  ;; values `-50' and `-46' represents the days before the easter.
  (dolist (count-days (number-sequence -50 -46))
    (add-to-list 'brazilian-holidays--local-holidays
		 `(holiday-easter-etc ,count-days "Carnaval")))

  (brazilian-holidays-mode t))

;;; org-roam basic config

;; Lazy load configurations for org-roam
(use-package org-roam
  :ensure t
  :requires (emacsql-sqlite3)
  :custom
  (org-roam-node-display-template (concat "${title:*} " (propertize "${tags:10}" 'face 'org-tag)))
  (org-roam-database-connector 'sqlite3)
  (org-roam-v2-ack t)
  (org-roam-completion-everywhere t)
  :config
  ;; Create org-roam directory
  (make-directory org-roam-directory t)

  (require 'org-roam-protocol)
  (org-roam-db-autosync-mode))

;;; Org Roam dailies

(use-package org-roam
  :custom
  ;; Templates p/ roam dailies
  ;; daily
  ;;   Shortcut:      d
  ;;   Nome:          daily
  ;;   Tipo:          "entry"
  ;;   Formato entry: Heading -> horario -> texto
  ;;   Arquivo:       agenda-{data}.org
  (org-roam-dailies-capture-templates
   (let ((agenda-file "agenda-%<%Y-%m>.org")
         (header "#+TITLE: Agenda %<%Y-%m>

#+STARTUP: content

* Journal [%<%Y-%m-%d>]"))
                  `(("j" "Journal [%<%Y-%m-%d>]" entry "* %<%H:%M> %?"
                     :target (file+head+olp ,agenda-file ,header ("Journal [%<%Y-%m-%d>]"))))))
  :config
  (require 'org-roam-dailies)

  (defun org-roam-dailies (dailies-fn)
    "Execute `org-roam-dailies' functions.

Interactive read the symbol `org-roam-dailies-map' and prompt all
options as multiple-choice."
    (interactive
     (let* ((choice-list (mapcar (lambda (kv)
				   (pcase-let* ((`(,key . ,value) kv)
						(value (symbol-name value)))
				     (list key (substring value 17))))
				 (cdr org-roam-dailies-map)))
	    (key-selected (car (read-multiple-choice "Open which daily type?" choice-list))))
       (list (alist-get key-selected (cdr org-roam-dailies-map)))))

    (message "Running function: %s" dailies-fn)
    (funcall dailies-fn))

  :bind ("C-c d" . org-roam-dailies))

;;; org.el ends here
