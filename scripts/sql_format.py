"""
This script reads an SQL text input the format to a pretty form.

So this:

```
SELECT * FROM accounts WHERE age > 5 AND active IS true;
```

Becomes this:

```
SELECT *
FROM accounts
WHERE age > 5
  AND active IS true;
```

"""

import argparse
import sqlparse
import sys

parser = argparse.ArgumentParser()
parser.add_argument('--wrap-after', dest='wrap_after', type=int, nargs='?', default=80)
parser.add_argument('--keyword-case', dest='keyword_case', nargs='?', choices=('upper', 'lower', 'capitalize'))

opt = parser.parse_args()

kw = dict(
    reindent=True,
    reindent_aligned=False,
    indent_tabs=False,
    wrap_after=opt.wrap_after,
    use_space_around_operators=True,
    keyword_case=opt.keyword_case,
    comma_first=False
)

print(sqlparse.format(sys.stdin.read(), **kw))
